Hi!
Schön, dass du zu meinem Plugin gekommen bist! 
Nur im allgemeinen hier einige Informationen: 
Der Coder (Gleydar) behält sich alle Rechte für dieses Plugin vor.
Das beinhaltet auch, dass bestimmte IP-Adressen von der Benutzung des Plugins ausgeschlossen werden.
Mit der Benutzung dieses Plugins erklärst du dich damit einverstanden, dass
Daten des Servers (IP-Adresse) auf einen Fremdrechner mit einer sicheren Verbindung übertragen werden. 