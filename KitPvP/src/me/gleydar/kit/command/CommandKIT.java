package me.gleydar.kit.command;

import me.gleydar.kit.kits.KitGLEYDAR;
import me.gleydar.kit.kits.KitHERUL;
import me.gleydar.kit.kits.KitLISKARIO;
import me.gleydar.kit.main.GuiMenu;
import me.gleydar.kit.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * 
 * @author Gleydar
 */

public class CommandKIT {


   String[] args;
   CommandSender sender;
   private final Main plugin;
   
    public CommandKIT(CommandSender sender, String[] args, Main instance){
        this.args = args;
        this.sender = sender;
        this.plugin = instance;
    }

    public boolean command(){
       if(sender instanceof Player){
           Player player = (Player) sender;
           
         
        if(args.length == 0){
        	if(!Main.kit.containsKey(player.getUniqueId())){
        		if(player.hasPermission("kitpvp.premium") || player.hasPermission("speedhunter.choose") || player.hasPermission("enderman.choose") || player.hasPermission("creeper.choose")){
        			GuiMenu.openChangerMenu(player);
        		}else{
        			GuiMenu.openNormalMenu(player);
        		}
        	}else{
        		player.sendMessage(plugin.surfIt("Don't change your class while playing :("));
        	}
            }

       if(args.length > 0){
    	   if(!Main.kit.containsKey(player.getUniqueId())){
        switch(args[0].toLowerCase()){
        	
            case "gleydar": if(player.getDisplayName().equalsIgnoreCase("gleydar")){
                KitGLEYDAR.kit(player);
            }else{
                player.sendMessage(plugin.surfIt("I do not approve."));
            }
               break; 
            case "herul": if(player.getDisplayName().equalsIgnoreCase("herul")){
            	KitHERUL.kit(player);
            }else{
            	player.sendMessage(plugin.surfIt("Das Keksmonster wird dich jagen"));
            }
            	break;
            case "liskario": if(player.getDisplayName().equalsIgnoreCase("liskario")){
            	KitLISKARIO.kit(player);
            }else{
            	player.sendMessage(plugin.surfIt("Schnuffel di schnuff?"));
            }
            case "leave": Main.kit.remove(player.getUniqueId());
            player.teleport(new Location(Bukkit.getWorld("world"), 200, 70, 200));
            player.sendMessage(plugin.surfIt("You let KitPvP"));
            	break;
            default: player.sendMessage(plugin.surfIt("Die Klasse, die du angegeben hast, existiert nicht"));
       }
                 
        
       }else{
    	   player.sendMessage(plugin.surfIt("Please don't change your class while playing :("));
       }
       }
       
        
       }else{
           sender.sendMessage("Netter Versuch, aber das geht nur als Spieler");
       }
        return true;
    }    

}
