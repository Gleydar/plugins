package me.gleydar.kit.main;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GuiMenu {
	static Inventory inv;
	static ItemStack item;
	static ItemMeta meta;
	static List<String> lore = new ArrayList<>(6);
	
	
	public static void openNormalMenu(Player player){
		
		inv = Bukkit.getServer().createInventory(null, 9 , ChatColor.RED + "Kit-Auswahl");
		item = new ItemStack(Material.BOW);
		meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GREEN+"Bogenschütze"+ChatColor.RESET);
        lore.add(ChatColor.GREEN + "Der Bogenschütze is ein");
        lore.add(ChatColor.GREEN + "hervorragender Fernkämpfer.");
        lore.add(ChatColor.GREEN + "Im Nahkampf allerdings...");
        meta.setLore(lore);
        item.setItemMeta(meta);
    inv.setItem(0, item);
		item = new ItemStack(Material.IRON_SWORD);
		meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_BLUE+"Soldat"+ChatColor.RESET);
        lore.clear();
        lore.add(ChatColor.BLUE + "Der Soldat ist ein guter");
        lore.add(ChatColor.BLUE + "Allrounder. Vorallem mit seiner");
        lore.add(ChatColor.BLUE + "Geschwindigkeit kann er punkten.");
        meta.setLore(lore);
        item.setItemMeta(meta);
    inv.setItem(3, item);
		item = new ItemStack(Material.DIAMOND_CHESTPLATE);
		meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_AQUA+"Tank"+ChatColor.RESET);
        lore.clear();
        lore.add(ChatColor.AQUA + "Der Tank ist ein guter");
        lore.add(ChatColor.AQUA + "Verteidiger und kann viel");
        lore.add(ChatColor.AQUA + "einstecken. Allerdings ist");
        lore.add(ChatColor.AQUA + "er sehr langsam.");
        meta.setLore(lore);
        item.setItemMeta(meta);
    inv.setItem(6, item);
    	item = new ItemStack(Material.SADDLE);
    	meta = item.getItemMeta();
    	meta.setDisplayName(ChatColor.DARK_GRAY + "Ritter");
    	lore.clear();
    	lore.add(ChatColor.GRAY + "Der Ritter hat ein Pferd");
    	lore.add(ChatColor.GRAY + "zu seiner Unterstützung.");
    	lore.add(ChatColor.GRAY + "Seine Ausrüstung ist allerdings");
    	lore.add(ChatColor.GRAY + "nicht sonderlich gut.");
    	meta.setLore(lore);
    	item.setItemMeta(meta);
    inv.setItem(8, item);
	player.openInventory(inv);
	Main.opened.add(player);
	
	}
	
	public static void openChangerMenu(Player player){
			inv = Bukkit.getServer().createInventory(null, 18 , ChatColor.RED + "Kit-Auswahl");
		item = new ItemStack(Material.BOW);
			meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.DARK_GREEN+"Bogenschütze"+ChatColor.RESET);
            lore.add(ChatColor.GREEN + "Der Bogenschütze is ein");
            lore.add(ChatColor.GREEN + "hervorragender Fernkämpfer.");
            lore.add(ChatColor.GREEN + "Im Nahkampf allerdings...");
            meta.setLore(lore);
            item.setItemMeta(meta);
        inv.setItem(0, item);
		item = new ItemStack(Material.IRON_SWORD);
			meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.DARK_BLUE+"Soldat"+ChatColor.RESET);
            lore.clear();
            lore.add(ChatColor.BLUE + "Der Soldat ist ein guter");
            lore.add(ChatColor.BLUE + "Allrounder. Vorallem mit seiner");
            lore.add(ChatColor.BLUE + "Geschwindigkeit kann er punkten.");
            meta.setLore(lore);
            item.setItemMeta(meta);
        inv.setItem(3, item);
		item = new ItemStack(Material.DIAMOND_CHESTPLATE);
			meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.DARK_AQUA+"Tank"+ChatColor.RESET);
            lore.clear();
            lore.add(ChatColor.AQUA + "Der Tank ist ein guter");
            lore.add(ChatColor.AQUA + "Verteidiger und kann viel");
            lore.add(ChatColor.AQUA + "einstecken. Allerdings ist");
            lore.add(ChatColor.AQUA + "er sehr langsam.");
            meta.setLore(lore);
            item.setItemMeta(meta);
        inv.setItem(6, item);
        	item = new ItemStack(Material.SADDLE);
        	meta = item.getItemMeta();
        	meta.setDisplayName(ChatColor.DARK_GRAY + "Ritter");
        	lore.clear();
        	lore.add(ChatColor.GRAY + "Der Ritter hat ein Pferd");
        	lore.add(ChatColor.GRAY + "zu seiner Unterstützung.");
        	lore.add(ChatColor.GRAY + "Seine Ausrüstung ist allerdings");
        	lore.add(ChatColor.GRAY + "nicht sonderlich gut.");
    		meta.setLore(lore);
    	item.setItemMeta(meta);
    	inv.setItem(8, item);
        item = new ItemStack(Material.DIAMOND);
        	meta = item.getItemMeta();
        	meta.setDisplayName(ChatColor.GOLD+"Premiumkit-Menü");
        	item.setItemMeta(meta);
        inv.setItem(17, item);
		player.openInventory(inv);
		Main.opened.add(player);
	}
	
	public static void openVipMenu(Player player){
		inv = Bukkit.getServer().createInventory(null, 9, ChatColor.GOLD + "Premiumkit-Auswahl");
	  item  = new ItemStack(Material.ENDER_PEARL);
		meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.DARK_AQUA + "Enderman" + ChatColor.RESET);
		lore.clear();
		lore.add(ChatColor.AQUA + "Der Enderman ist ein schneller");
		lore.add(ChatColor.AQUA + "Kämpfer. Mit seiner Teleportation");
		lore.add(ChatColor.AQUA + "kann er sich aus brenzligen Situationen retten.");
		lore.add(ChatColor.AQUA + "Mit seiner Rüstung kann er");
		lore.add(ChatColor.AQUA + "allerdings nicht punkten.");
		meta.setLore(lore);
		item.setItemMeta(meta);
	if(player.hasPermission("kitpvp.premium") || player.hasPermission("enderman.choose")){	
	  inv.setItem(0, item);
	}
	  item = new ItemStack(Material.WOOD_SWORD);
	  	meta = item.getItemMeta();
	  	meta.setDisplayName(ChatColor.GOLD + "Speedhunter" + ChatColor.RESET);
	  	lore.clear();
	  	lore.add(ChatColor.YELLOW + "Der Speedhunter ist sehr");
	  	lore.add(ChatColor.YELLOW + "schnell und hat einen guten");
	  	lore.add(ChatColor.YELLOW + "Bogen. Allerdings muss auch er");
	  	lore.add(ChatColor.YELLOW + "bei seiner Rüstung aufpassen.");
	  	meta.setLore(lore);
	  	item.setItemMeta(meta);
	if(player.hasPermission("kitpvp.premium") || player.hasPermission("speedhunter.choose")){
	  inv.setItem(3, item);
	}
	  item = new ItemStack(Material.SULPHUR);
	  	meta = item.getItemMeta();
	  	meta.setDisplayName(ChatColor.DARK_GREEN + "Creeper" + ChatColor.RESET);
	  	lore.clear();
	  	lore.add(ChatColor.GREEN + "Der Creeper kann mit seinen");
	  	lore.add(ChatColor.GREEN + "Explosionen auch viele Gegner");
	  	lore.add(ChatColor.GREEN + "bekämpfen. Allerdings sind");
	  	lore.add(ChatColor.GREEN + "diese auch begrenzt...");
	  	meta.setLore(lore);
	  	item.setItemMeta(meta);
	if(player.hasPermission("kitpvp.premium") || player.hasPermission("creeper.choose")){
	  inv.setItem(6, item);
	}
	  player.openInventory(inv);
	  Main.opened.add(player);
	}
	
}
