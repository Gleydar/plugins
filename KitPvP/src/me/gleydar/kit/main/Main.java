
package me.gleydar.kit.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import me.gleydar.kit.command.CommandKIT;
import me.gleydar.kit.event.PlayerDeath;
import me.gleydar.kit.event.PlayerInteract;
import me.gleydar.kit.event.PlayerInventory;
import me.gleydar.kit.event.PlayerRightClick;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;


/**
 * 
 * @author Gleydar
 */

public class Main extends JavaPlugin{

   Command cmd;
   String[] args;
   CommandSender sender;
   public static final Logger log = Bukkit.getLogger();
   public static Map<UUID, String> kit = new HashMap<UUID, String>();
   public static List<Player> opened = new ArrayList<>();
   public int PID;

   
   @Override
   public void onEnable(){
	   updateCheck();
       registerEvents();
       log.info("[KitPvP] by Gleydar enabled");
   }
   
   @Override
   public void onDisable(){
	   log.info("[KitPvP] by Gleydar disabled");
	   Bukkit.getScheduler().cancelTask(PID);
   }
   
   @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args){
       if(cmd.getName().equalsIgnoreCase("kit")){
           return new CommandKIT(sender, args, this).command();
       }
        return false;
   }
   
   public String surfIt(String message){
       message = ChatColor.GREEN+"["+ChatColor.DARK_AQUA+ChatColor.BOLD+"KitPvP"+ChatColor.RESET+ChatColor.GREEN+"] "+ChatColor.GREEN+ message;
       return message;
   }
   
   private void registerEvents(){
       Bukkit.getPluginManager().registerEvents(new PlayerDeath(), this);
       Bukkit.getPluginManager().registerEvents(new PlayerRightClick(), this);
       Bukkit.getPluginManager().registerEvents(new PlayerInteract(), this);
       Bukkit.getPluginManager().registerEvents(new PlayerInventory(), this);
   }
   
   public void updateCheck(){
		 try {
			   BufferedReader r = new BufferedReader(new InputStreamReader(new URL("https://dl.dropboxusercontent.com/u/55669684/KitPvP-Update.txt").openStream()));
			   String str = r.readLine();
			   	if(!str.equalsIgnoreCase("Neueste Version: " + getDescription().getVersion())){
			   		Player[] players = Bukkit.getOnlinePlayers();
			   		for(int i = 0; i < players.length; i++){
			   			if(players[i].hasPermission("kitpvp.recive")){
			   				players[i].sendMessage(surfIt(ChatColor.RED + "Es ist ein neues Update verfügbar!"));
			   				players[i].sendMessage(surfIt(str) + " Deine Version: " + getDescription().getVersion());
			   			while((str = r.readLine()) != null) {
			   				log.info(str);
			   		    		players[i].sendMessage(surfIt(str));
			   		    	}
			   		    }
			   	   }
			   	}
			   r.close();
		 }catch(IOException e){
			 log.info("Update-Info konnte nicht geladen werden.");
		 }
   }
   
   public static void clearInventory(Player player){
	   		player.removePotionEffect(PotionEffectType.SPEED);
       		player.removePotionEffect(PotionEffectType.SLOW);
       		player.removePotionEffect(PotionEffectType.FAST_DIGGING);
       		player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
       		player.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
       		player.removePotionEffect(PotionEffectType.HEAL);
       		player.removePotionEffect(PotionEffectType.HARM);
       		player.removePotionEffect(PotionEffectType.JUMP);
       		player.removePotionEffect(PotionEffectType.CONFUSION);
       		player.removePotionEffect(PotionEffectType.REGENERATION);
       		player.removePotionEffect(PotionEffectType.INVISIBILITY);
       		player.removePotionEffect(PotionEffectType.BLINDNESS);
       		player.removePotionEffect(PotionEffectType.NIGHT_VISION);
       		player.removePotionEffect(PotionEffectType.HUNGER);
       		player.removePotionEffect(PotionEffectType.WEAKNESS);
       		player.removePotionEffect(PotionEffectType.POISON);
       		player.removePotionEffect(PotionEffectType.WEAKNESS);
       		player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
       		player.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
       		player.removePotionEffect(PotionEffectType.WITHER);
	        player.getInventory().clear();
	        player.getInventory().setBoots(null);
	        player.getInventory().setChestplate(null);
	        player.getInventory().setHelmet(null);
	        player.getInventory().setLeggings(null);
	        player.setGameMode(GameMode.ADVENTURE);
	        ((Damageable)player).setHealth(20.0F);
	        player.setFoodLevel(20);
	    }  
  
   
}