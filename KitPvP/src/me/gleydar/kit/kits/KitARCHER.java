package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class KitARCHER {

	
	public static void kit(Player player){
        ItemStack item;
        PlayerInventory inventory = player.getInventory();
        
           me.gleydar.kit.main.Main.clearInventory(player);
            
           item = new ItemStack(Material.BOW);
           item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
           item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
           item.addUnsafeEnchantment(Enchantment.ARROW_KNOCKBACK, 1);
           item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
           ItemMeta meta = item.getItemMeta();
           meta.setDisplayName("Kurzbogen");
           item.setItemMeta(meta);
           inventory.addItem(item);
           
           item = new ItemStack(Material.ARROW);
           inventory.addItem(item);
           
           item = new ItemStack(Material.PAPER, 3);
           meta = item.getItemMeta();
           meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
           item.setItemMeta(meta);
           inventory.addItem(item);
           
           item = new ItemStack(Material.CHAINMAIL_HELMET);
           inventory.setHelmet(item);
           
           item = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
           item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
           inventory.setChestplate(item);
           
           item = new ItemStack(Material.CHAINMAIL_LEGGINGS);
           inventory.setLeggings(item);
           
           item = new ItemStack(Material.CHAINMAIL_BOOTS);
           item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
           inventory.setBoots(item);
           
           Main.kit.remove(player.getUniqueId());
           Main.kit.put(player.getUniqueId(), "archer");
        
    }
}
