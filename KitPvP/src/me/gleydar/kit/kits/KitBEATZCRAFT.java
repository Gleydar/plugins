package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class KitBEATZCRAFT {

	
	public static void kit(Player player){
        ItemStack item;
        PlayerInventory inventory = player.getInventory();
        
           Main.clearInventory(player);
           
           item = new ItemStack(Material.PISTON_MOVING_PIECE);
           item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
           item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
           ItemMeta meta = item.getItemMeta();
           meta.setDisplayName(ChatColor.DARK_RED+""+ChatColor.BOLD+"OP-Piston Kopf"+ChatColor.RESET);
           item.setItemMeta(meta);
           inventory.addItem(item);
                
           item = new ItemStack(Material.BOW);
           item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
           item.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 2);
           meta = item.getItemMeta();
           meta.setDisplayName(ChatColor.DARK_RED+""+ChatColor.BOLD+"OP-Bogen"+ChatColor.RESET);
           item.setItemMeta(meta);
           inventory.addItem(item);
           
           item = new ItemStack(Material.ARROW);
           inventory.addItem(item);
           
           	Main.kit.remove(player.getUniqueId());
            Main.kit.put(player.getUniqueId(), "beatzcraft");
    }
	
}
