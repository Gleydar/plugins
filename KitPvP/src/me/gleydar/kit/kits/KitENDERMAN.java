package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class KitENDERMAN {

	public static void kit(Player player){
        ItemStack item;
        PlayerInventory inventory = player.getInventory();
        ItemMeta meta;
        
        Main.clearInventory(player);
               
        item = new ItemStack(Material.IRON_SWORD);
        item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
        item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
        meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE+"Enderschwert"+ChatColor.RESET);
        item.setItemMeta(meta);
        inventory.addItem(item);
        
        item = new ItemStack(Material.PAPER, 1);
        meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
        item.setItemMeta(meta);
        inventory.addItem(item);
        
        item = new ItemStack(Material.IRON_HELMET);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        inventory.setHelmet(item);
        
        item = new ItemStack(Material.LEATHER_CHESTPLATE);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        LeatherArmorMeta lam = (LeatherArmorMeta)item.getItemMeta();
        lam.setColor(Color.fromRGB(0, 0, 0));
        item.setItemMeta(lam);
        inventory.setChestplate(item);
        
        item = new ItemStack(Material.LEATHER_LEGGINGS);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        lam = (LeatherArmorMeta)item.getItemMeta();
        lam.setColor(Color.fromRGB(0, 0, 0));
        item.setItemMeta(lam);
        inventory.setLeggings(item);
        
        item = new ItemStack(Material.LEATHER_BOOTS);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        lam = (LeatherArmorMeta)item.getItemMeta();
        lam.setColor(Color.fromRGB(0, 0, 0));
        item.setItemMeta(lam);
        inventory.setBoots(item);
        
        Main.kit.remove(player.getUniqueId());
        Main.kit.put(player.getUniqueId(), "enderman");
    }
}
