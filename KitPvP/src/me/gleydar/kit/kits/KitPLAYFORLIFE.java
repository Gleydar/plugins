package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class KitPLAYFORLIFE {

	static Main plugin;
	
    public static void kit(Player player){
        ItemStack item;
        PlayerInventory inventory = player.getInventory();
        
           
           Main.clearInventory(player);
           
           item = new ItemStack(Material.RED_ROSE);
           item.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 2);
           item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
           ItemMeta meta = item.getItemMeta();
           meta.setDisplayName(ChatColor.LIGHT_PURPLE+"YouTuber"+ChatColor.WHITE+"-Rose");
           item.setItemMeta(meta);
           inventory.addItem(item);
           
         item = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
         item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
         inventory.setChestplate(item);
        
         Main.kit.remove(player.getUniqueId());
         Main.kit.put(player.getUniqueId(), "play4life");
    }
	
}
