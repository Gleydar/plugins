package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class KitTANK {
	
	static Main plugin;
	
	public KitTANK(){
		
	}
	
	public static void kit(Player player){
        ItemStack item;
        PlayerInventory inventory = player.getInventory();
        PotionEffect po;
        

            Main.clearInventory(player);
            
            item = new ItemStack(Material.IRON_SWORD);
            item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("Langschwert");
            item.setItemMeta(meta);
            inventory.addItem(item);
            
            item = new ItemStack(Material.DIAMOND_CHESTPLATE);
            item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
            inventory.setChestplate(item);
           
            item = new ItemStack(Material.IRON_LEGGINGS);
            inventory.setLeggings(item);   
            
            po = new PotionEffect(PotionEffectType.SLOW, 50000, 2);
            player.addPotionEffect(po);
            
            Main.kit.remove(player.getUniqueId());
            Main.kit.put(player.getUniqueId(), "tank");
    }
}
