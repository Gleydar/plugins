package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class KitSPEEDHUNTER {

	static Main plugin;
	
	public static void kit(Player player){
        ItemStack item;
        PlayerInventory inventory = player.getInventory();
        PotionEffect po;
        
            Main.clearInventory(player);
            
            item = new ItemStack(Material.BOW);
            item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 2);
            item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
            item.addUnsafeEnchantment(Enchantment.ARROW_KNOCKBACK, 2);
            item.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 1);
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.GOLD+"J�ger-Bogen"+ChatColor.RESET);
            item.setItemMeta(meta);
            inventory.addItem(item);
           
            item = new ItemStack(Material.WOOD_SWORD);
            item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
            meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.GOLD+"J�ger-Dolch"+ChatColor.RESET);
            item.setItemMeta(meta);
            inventory.addItem(item);
            
            item = new ItemStack(Material.ARROW);
            inventory.addItem(item);
            
            item = new ItemStack(Material.PAPER, 3);
            meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
            item.setItemMeta(meta);
            inventory.addItem(item);
            
            item = new ItemStack(Material.CHAINMAIL_HELMET);
            inventory.setHelmet(item);
           
            item = new ItemStack(Material.IRON_CHESTPLATE);
            inventory.setChestplate(item);
           
            item = new ItemStack(Material.CHAINMAIL_LEGGINGS);
            inventory.setLeggings(item);
           
            item = new ItemStack(Material.CHAINMAIL_BOOTS);
            inventory.setBoots(item);
           
            po = new PotionEffect(PotionEffectType.SPEED,50000,2);
            player.addPotionEffect(po);
            
            Main.kit.remove(player.getUniqueId());
            Main.kit.put(player.getUniqueId(), "speedhunter");
    }
}
