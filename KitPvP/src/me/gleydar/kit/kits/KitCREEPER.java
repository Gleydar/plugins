package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class KitCREEPER {

	static Main plugin;
	
    public static void kit(Player player){
   	 Main.clearInventory(player);
   	
   	 ItemStack item = new ItemStack(Material.SULPHUR, 20);
   	 ItemMeta meta = item.getItemMeta();
   	 meta.setDisplayName(ChatColor.DARK_GREEN+"Explosion!");
   	 item.setItemMeta(meta);
   	 player.getInventory().addItem(item);
   	 
   	 item = new ItemStack(Material.IRON_SWORD);
        item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
        item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
        meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GREEN+"Creeperschwert"+ChatColor.RESET);
        item.setItemMeta(meta);
        player.getInventory().addItem(item);
   	 
        item = new ItemStack(Material.PAPER, 2);
        meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
        item.setItemMeta(meta);
        player.getInventory().addItem(item);
        
   	 item = new ItemStack(Material.LEATHER_CHESTPLATE);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        LeatherArmorMeta lam = (LeatherArmorMeta)item.getItemMeta();
        lam.setColor(Color.fromRGB(0, 100, 5));
        item.setItemMeta(lam);
        player.getInventory().setChestplate(item);
        
        item = new ItemStack(Material.LEATHER_LEGGINGS);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        lam = (LeatherArmorMeta)item.getItemMeta();
        lam.setColor(Color.fromRGB(0, 100, 5));
        item.setItemMeta(lam);
        player.getInventory().setLeggings(item);
        
        item = new ItemStack(Material.LEATHER_BOOTS);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        lam = (LeatherArmorMeta)item.getItemMeta();
        lam.setColor(Color.fromRGB(0, 100, 5));
        item.setItemMeta(lam);
        player.getInventory().setBoots(item);
        
        Main.kit.remove(player.getUniqueId());
        Main.kit.put(player.getUniqueId(), "creeper");
   }
}
