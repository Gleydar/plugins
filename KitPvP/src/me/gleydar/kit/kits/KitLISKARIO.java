package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class KitLISKARIO {

	static Main plugin;
	
    public static void kit(Player player){
        Main.clearInventory(player);
        PlayerInventory inventory = player.getInventory();
        
        ItemStack item = new ItemStack(Material.EGG);
        item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 10);
        item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.BLUE + "Megaeier" + ChatColor.RESET);
        item.setItemMeta(meta);
        inventory.addItem(item);
        
        item = new ItemStack(Material.BEDROCK);
        inventory.setHelmet(item);
        
        item = new ItemStack(Material.PAPER, 64);
        meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
        item.setItemMeta(meta);
        inventory.addItem(item);
        
        item = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        inventory.setChestplate(item);
        
        Main.kit.remove(player.getUniqueId());
        Main.kit.put(player.getUniqueId(), "liskario");
    }
}
