package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class KitKNIGHT {

	static Main plugin;
	
	 public static void kit(Player player){
	        ItemStack item;
	        PlayerInventory inventory = player.getInventory();
	           Main.clearInventory(player);
	           
	            Horse h = (Horse) player.getWorld().spawnEntity(player.getLocation(), EntityType.HORSE);
	            h.getInventory().addItem(new ItemStack(Material.SADDLE));
	            h.getInventory().addItem(new ItemStack(Material.GOLD_BARDING));
	            h.setCustomName(player.getDisplayName() + "'s Pferd");
	            h.setPassenger(player);
	           
	            item = new ItemStack(Material.IRON_SWORD);
	            item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
	            item.addUnsafeEnchantment(Enchantment.DURABILITY, 8);
	            ItemMeta meta = item.getItemMeta();
	            meta.setDisplayName("Kavallerieschwert");
	            item.setItemMeta(meta);
	            inventory.addItem(item);
	           
	            item = new ItemStack(Material.LEATHER_HELMET);
	            inventory.setHelmet(item);	           
	           
	            item = new ItemStack(Material.LEATHER_BOOTS);
	            inventory.setBoots(item);
	            
	            item = new ItemStack(Material.PAPER, 1);
	            meta = item.getItemMeta();
	            meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
	            item.setItemMeta(meta);
	            inventory.addItem(item);	          
	            
	            Main.kit.remove(player.getUniqueId());
	            Main.kit.put(player.getUniqueId(), "knight");
	    }
	    
	
}
