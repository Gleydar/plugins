package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class KitICERIDER {

	static Main plugin;
	
    public static void kit(Player player){
        Main.clearInventory(player);
        PlayerInventory inventory = player.getInventory();
        
        ItemStack item = new ItemStack(Material.ICE);
        item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
        item.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_AQUA+""+ChatColor.BOLD+"Ice, Ice Baby"+ChatColor.RESET);
        item.setItemMeta(meta);
        inventory.addItem(item);
        
        item = new ItemStack(Material.PAPER, 3);
        meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
        item.setItemMeta(meta);
        inventory.addItem(item);
        
        item = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        inventory.setChestplate(item);
        
        Main.kit.remove(player.getUniqueId());
        Main.kit.put(player.getUniqueId(), "ice");
    }
	
}
