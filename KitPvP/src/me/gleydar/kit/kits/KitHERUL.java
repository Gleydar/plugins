package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class KitHERUL {

	static Main plugin;
	
    public static void kit(Player player){
    	Main.clearInventory(player);
    	
    	ItemStack item = new ItemStack(Material.COOKIE, 1);
    	item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 10);
    	item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
    	ItemMeta meta = item.getItemMeta();
    	meta.setDisplayName(ChatColor.DARK_BLUE + "Cookie-Power" + ChatColor.RESET);
    	item.setItemMeta(meta);
    	player.getInventory().addItem(item);
    	
    	
    	item = new ItemStack(Material.PAPER, 3);
        meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
        item.setItemMeta(meta);
        player.getInventory().addItem(item);
    	
    	 item = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
         item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
         player.getInventory().setChestplate(item);
         
         Main.kit.remove(player.getUniqueId());
         Main.kit.put(player.getUniqueId(), "herul");
    	}

}
