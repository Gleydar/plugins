package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class KitHOUSEVERBOT {

	static Main plugin;
	
	public static void kit(Player player){
        ItemStack item;
        PlayerInventory inventory = player.getInventory();
        
            Main.clearInventory(player);
            
            item = new ItemStack(Material.IRON_DOOR);
            item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 6);
            item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.DARK_PURPLE+""+ChatColor.BOLD+"Du hast Houseverbot!"+ChatColor.RESET);
            item.setItemMeta(meta);
            inventory.addItem(item);
            
            item = new ItemStack(Material.IRON_CHESTPLATE);
            inventory.setHelmet(item);
            
            item = new ItemStack(Material.LEATHER_HELMET);
            inventory.setChestplate(item);
            
            Main.kit.remove(player.getUniqueId());
            Main.kit.put(player.getUniqueId(), "houseverbot");
    }
	
}
