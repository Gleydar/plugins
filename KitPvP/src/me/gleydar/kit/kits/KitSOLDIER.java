package me.gleydar.kit.kits;

import me.gleydar.kit.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class KitSOLDIER {

	static Main plugin;
	
	 public static void kit(Player player){
	        ItemStack item;
	        PlayerInventory inventory = player.getInventory();
	        PotionEffect po;
	        

	           Main.clearInventory(player);
	           
	           item = new ItemStack(Material.IRON_SWORD);
	           item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
	           item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
	           ItemMeta meta = item.getItemMeta();
	           meta.setDisplayName("Kurzschwert");
	           item.setItemMeta(meta);
	           inventory.addItem(item);
	           
	            item = new ItemStack(Material.LEATHER_HELMET);
	            inventory.setHelmet(item);
	           
	            item = new ItemStack(Material.IRON_CHESTPLATE);
	            inventory.setChestplate(item);
	           
	            item = new ItemStack(Material.PAPER, 2);
	            meta = item.getItemMeta();
	            meta.setDisplayName(ChatColor.AQUA + "Pflaster" + ChatColor.RESET);
	            item.setItemMeta(meta);
	            inventory.addItem(item);
	            
	            item = new ItemStack(Material.IRON_LEGGINGS);
	            inventory.setLeggings(item);
	           
	            item = new ItemStack(Material.IRON_BOOTS);
	            inventory.setBoots(item);
	            
	            po = new PotionEffect(PotionEffectType.SPEED, 50000, 1);
	            player.addPotionEffect(po);
	            
	            Main.kit.remove(player.getUniqueId());
	            Main.kit.put(player.getUniqueId(), "soldier");
	    }
	    
	
}
