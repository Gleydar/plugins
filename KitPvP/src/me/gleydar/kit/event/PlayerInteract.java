

package me.gleydar.kit.event;


import me.gleydar.kit.main.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.util.Vector;

/**
 *
 * @author Gleydar
 */
    
    


public class PlayerInteract implements Listener{
    
    
    @EventHandler(priority = EventPriority.HIGH)
public void onPlayerInteract(PlayerMoveEvent event){
    final Player player = event.getPlayer();
      Location loc = event.getTo();
      player.setFoodLevel(20);
      Block block2 = (Block) loc.getBlock().getRelative(0, -1, 0);
      
      if (Material.STATIONARY_WATER == block2.getType()) {
          if(!player.isSneaking()){
        Vector vector = new Vector(0.0D, 1.5D, 0.0D);
        player.setVelocity(vector);
      }
      }
      
      
      
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerPickup(PlayerPickupItemEvent ev){
    	if(Main.kit.containsKey(ev.getPlayer().getName())){
    		ev.setCancelled(true);
    	}
    }
  }
    
