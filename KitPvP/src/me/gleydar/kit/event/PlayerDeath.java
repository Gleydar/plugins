
package me.gleydar.kit.event;


import me.gleydar.kit.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;



/**
 * 
 * @author Gleydar
 */

public class PlayerDeath implements Listener{
      
	
    public PlayerDeath(){
        
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    	public void onDeath(PlayerDeathEvent ev){
    		Player player = ev.getEntity();
    		Player killer = player.getKiller();
    		
    		if(Main.kit.containsKey(player.getUniqueId())){
    			ev.setKeepLevel(true);
    			killer.setLevel(killer.getLevel() + 1);
    			player.playSound(player.getLocation(), Sound.GHAST_MOAN, 1.0F, 1.0F);
    			killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
    			
    			player.setHealth(20D);
    			Main.kit.remove(player.getUniqueId());
    			player.teleport(new Location(Bukkit.getWorld("world_the_nether"), 200, 70, 200));
    		}
    	
    }
    

    @EventHandler(priority = EventPriority.NORMAL)
    	public void onQuit(PlayerQuitEvent ev){
    		Player player = ev.getPlayer();
    		Main.kit.remove(player.getUniqueId());
    
    }

    @EventHandler(priority = EventPriority.NORMAL)
    	public void onDrop(PlayerDropItemEvent ev){
    		if(Main.kit.containsKey(ev.getPlayer().getUniqueId())){
    			ev.setCancelled(true);
    		}
    }

}
