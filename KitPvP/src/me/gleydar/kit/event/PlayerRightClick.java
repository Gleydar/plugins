

package me.gleydar.kit.event;

import java.util.ArrayList;

import me.gleydar.kit.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 *
 * @author Gleydar
 */

public class PlayerRightClick implements Listener {
    
	private ArrayList<Player> teleported = new ArrayList<Player>();
	
	@EventHandler(priority = EventPriority.HIGH)
    	public void onExpCollect(PlayerExpChangeEvent ev){
			if(Main.kit.containsKey(ev.getPlayer().getUniqueId())){
				ev.setAmount(0);
			}
		}


	@EventHandler(priority = EventPriority.HIGH)
		public void onDamageCauses(EntityDamageEvent ev){
			if(DamageCause.FALL == ev.getCause()){
					if(Main.kit.containsKey(ev.getEntity().getUniqueId())){
						ev.setCancelled(true);
					}
			}
			
			if(DamageCause.ENTITY_EXPLOSION == ev.getCause() || DamageCause.BLOCK_EXPLOSION == ev.getCause() || DamageCause.MAGIC == ev.getCause()){
				ev.setDamage(9.0F);
				
				if(ev.getEntity() instanceof Player){
					Player player = (Player)ev.getEntity();
					
					if(Main.kit.get(player.getUniqueId()) == "creeper"){
						ev.setDamage(0.0F);
					}
				}
			}
	}


  @EventHandler(priority = EventPriority.HIGH)
  	public void onRightClick(final PlayerInteractEvent ev){
	  
	  if(ev.getAction() == Action.RIGHT_CLICK_BLOCK){
		  final Player player = ev.getPlayer();
		  
		  if(Main.kit.get(player.getUniqueId()) == "enderman" && player.getItemInHand().getType() == Material.IRON_SWORD){
			  if(!teleported.contains(player.getName())){
    			   Location loc = ev.getClickedBlock().getLocation();
    			   loc.setPitch(player.getLocation().getPitch());
    			   loc.setYaw(player.getLocation().getYaw());
    			   player.teleport(loc.add(0.0F, 1.0F, 0.0F));
    			   player.getWorld().playEffect(loc, Effect.ENDER_SIGNAL, 1);
    			   player.getWorld().playSound(loc, Sound.ENDERMAN_TELEPORT, 1.2F, 1.0F);
    			   teleported.add(player);
    			   
    			   Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("KitPVP"), new Runnable(){

					@Override
					public void run() {
						teleported.remove(player.getUniqueId());
					}
    				   
    			   });
			  }
		  }
       
       if(Main.kit.get(player.getUniqueId()) == "creeper"){	   
    	   if(player.getItemInHand().getType() == Material.SULPHUR){    		   
    		   double h = ((Damageable)player).getHealth();
    		   
    		   player.getWorld().createExplosion(ev.getClickedBlock().getLocation().getX(), ev.getClickedBlock().getLocation().getY(), ev.getClickedBlock().getLocation().getZ(), 4.5F, false, false);
    		   
    		   player.setHealth(h);
    		   
    		   int ammount = player.getItemInHand().getAmount();
    		   if(ammount != 1){
    			   player.getItemInHand().setAmount(ammount - 1);
    		   }else{
    			   player.getInventory().remove(Material.SULPHUR);
    		   }
    	   }
       }
       
       
   }
	  
   if(ev.getAction() == Action.RIGHT_CLICK_AIR || ev.getAction() == Action.RIGHT_CLICK_BLOCK){
	   Player player = ev.getPlayer();
	   if(player.getItemInHand().getType() == Material.PAPER){
		   int ammount = player.getItemInHand().getAmount();
		   double health = ((Damageable) player).getHealth(); 
		   
		   health = health + 6;
		   
		   if(health > 20){
			   health = 20;
		   }
		   
		   if(ammount != 1){
			   player.getItemInHand().setAmount(ammount - 1);
		   }else{
			   player.getInventory().remove(Material.PAPER);
		   }
		   
		   player.playSound(player.getLocation(), Sound.WOLF_PANT, 2.0F, 1.0F);
		   ((Damageable)player).setHealth(health);
		   		   
       }
   }

    
}

}
