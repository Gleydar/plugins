package me.gleydar.kit.event;

import me.gleydar.kit.kits.KitARCHER;
import me.gleydar.kit.kits.KitCREEPER;
import me.gleydar.kit.kits.KitENDERMAN;
import me.gleydar.kit.kits.KitKNIGHT;
import me.gleydar.kit.kits.KitSOLDIER;
import me.gleydar.kit.kits.KitSPEEDHUNTER;
import me.gleydar.kit.kits.KitTANK;
import me.gleydar.kit.main.GuiMenu;
import me.gleydar.kit.main.Main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class PlayerInventory implements Listener{

 @EventHandler(priority = EventPriority.HIGH)
 public void onClick(InventoryClickEvent ev){
	 if(Main.opened.contains(ev.getWhoClicked())){
		 ev.setCancelled(true);
		 Player player = (Player)ev.getWhoClicked();
		 switch(ev.getCurrentItem().getType()){
		 	case BOW: KitARCHER.kit(player);
		 				player.closeInventory();
		 		break;
		 	case IRON_SWORD: KitSOLDIER.kit(player);
		 				player.closeInventory();
		 		break;
		 	case DIAMOND_CHESTPLATE: KitTANK.kit(player);
		 				player.closeInventory();
		 		break;
		 	case DIAMOND: GuiMenu.openVipMenu(player);
		 		break;
		 	case ENDER_PEARL: KitENDERMAN.kit(player);
		 				player.closeInventory();
		 		break;
		 	case WOOD_SWORD: KitSPEEDHUNTER.kit(player);
		 				player.closeInventory();
		 		break;
		 	case SULPHUR: KitCREEPER.kit(player);
		 				player.closeInventory();
		 		break;
		 	case SADDLE: KitKNIGHT.kit(player);
		 				player.closeInventory();
		 		break;
		default:
			break;
		 }
	 }
	 
 }
 
 
 
}
