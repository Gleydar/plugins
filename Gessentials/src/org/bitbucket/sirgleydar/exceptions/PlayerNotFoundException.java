package org.bitbucket.sirgleydar.exceptions;

import org.bukkit.ChatColor;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class PlayerNotFoundException extends Exception{

	private static final long serialVersionUID = 4826870243344953186L;
	
	public PlayerNotFoundException(String p, String command){
		super(ChatColor.GRAY + "[" + ChatColor.BLUE + command.toUpperCase() + ChatColor.GRAY + "] " + ChatColor.RED + '"' + p + '"' + " doesnt appears to be a valid player");
	}

}


