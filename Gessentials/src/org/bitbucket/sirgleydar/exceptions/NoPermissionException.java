package org.bitbucket.sirgleydar.exceptions;

import org.bukkit.ChatColor;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class NoPermissionException extends Exception{

	private static final long serialVersionUID = -2731213897296720263L;
	
	public NoPermissionException(){
		super(ChatColor.DARK_RED + "Tsss, Tsss, tsss...");
	}

}


