package org.bitbucket.sirgleydar.exceptions;

import org.bukkit.ChatColor;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class ArgumentException extends Exception{

	private static final long serialVersionUID = 3134939582584406070L;

	public ArgumentException(String command){
		super(ChatColor.GRAY + "[" + ChatColor.BLUE + command.toUpperCase() + ChatColor.GRAY + "] " + ChatColor.RED +  "Invalid Arguments");
	}
}


