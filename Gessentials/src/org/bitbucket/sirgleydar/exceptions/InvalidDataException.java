package org.bitbucket.sirgleydar.exceptions;

import org.bukkit.ChatColor;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class InvalidDataException extends Exception{

	private static final long serialVersionUID = -7398461537626121296L;

	public InvalidDataException(String command, String e, String d){
		super(ChatColor.GRAY + "[" + ChatColor.BLUE + command.toUpperCase() + ChatColor.GRAY + "] " + ChatColor.RED + '"' + e + '"' + " doesnt appears to be a valid " +  '"' + d + '"');
	}
}


