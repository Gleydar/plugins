package org.bitbucket.sirgleydar.main;

import java.util.logging.Logger;

import org.bitbucket.sirgleydar.commands.GCommandHandler;
import org.bitbucket.sirgleydar.events.PlayerChatListener;
import org.bitbucket.sirgleydar.events.PlayerJoinListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class Main extends JavaPlugin{

	
	public static Logger log = Bukkit.getLogger();
			
	@Override
	public void onEnable(){
		createConfig();
		registerEvents();
		log.info("Plugin by Gleydar vers. " + this.getDescription().getVersion() +" enabled");
	}
	
	 @Override
	 public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args){
		 
		 try {
			return GCommandHandler.handleCommand(sender, cmd, args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
		 
	 }
	
	@Override
	public void onDisable(){
		
	}
	
	public void registerEvents(){
		Bukkit.getPluginManager().registerEvents(new PlayerChatListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
	}
	
	public void createConfig(){
		
	}
	
	public static void sendMessage(String command, String msg, Player p){
		msg = msg.replace("%", ChatColor.GOLD+"");
		msg = msg.replace("&", ChatColor.BLUE+"");
		if(p!=null){
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + command.toUpperCase() + ChatColor.GRAY + "] " + ChatColor.BLUE+ msg);
		}else{
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "BROADCAST" + ChatColor.GRAY + "] " + ChatColor.BLUE+ msg);
		}
	}
	
}


