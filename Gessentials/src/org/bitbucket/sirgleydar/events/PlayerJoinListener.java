package org.bitbucket.sirgleydar.events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class PlayerJoinListener implements Listener{

	public PlayerJoinListener(){
		
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		PermissionUser pu = PermissionsEx.getUser(p);
	
		
		e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_BLUE + "[" + pu.getPrefix() + ChatColor.GRAY + "]"+ p.getDisplayName() + ChatColor.BLUE +" ist dem Server beigetreten"));
	}
	
}


