package org.bitbucket.sirgleydar.events;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class PlayerChatListener implements Listener{
	
	private HashMap<String, String> dinokin = new HashMap<String, String>();
	
	public PlayerChatListener(){
		
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		PermissionUser pu = PermissionsEx.getUser(p);
		
		dinokin.put("ladytherris", ChatColor.DARK_PURPLE +"Brodaak");
		dinokin.put("hyperion2510", ChatColor.BLACK + "Brodaak");
		dinokin.put("fox_alcarez", ChatColor.DARK_BLUE + "Vahlock");
		dinokin.put("maxjustin7", "");
		dinokin.put("gleydar", ChatColor.DARK_GREEN + "Techniker");
		
		if(dinokin.containsKey(p.getDisplayName().toLowerCase())){
			e.setFormat(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_GRAY + "" + ChatColor.MAGIC + ":;" + ChatColor.RESET + ChatColor.BLUE + "D" + ChatColor.RESET  + "" + ChatColor.DARK_GRAY + ChatColor.MAGIC + ":; " + ChatColor.RESET + "" + ChatColor.GRAY + "[" + dinokin.get(p.getDisplayName().toLowerCase())+ ChatColor.GRAY + "] " + ChatColor.DARK_GRAY + "%1$s" + ChatColor.GRAY + ": " + ChatColor.WHITE + "%2$s")); 
		}else{
			e.setFormat(ChatColor.translateAlternateColorCodes('&', ChatColor.GRAY + "[" + pu.getPrefix() + ChatColor.GRAY + "] " + ChatColor.DARK_GRAY + "%1$s" + ChatColor.GRAY + ": " + ChatColor.WHITE + "%2$s")); 
		}
	}
	
}


