package org.bitbucket.sirgleydar.commands;

import org.bitbucket.sirgleydar.exceptions.InvalidDataException;
import org.bitbucket.sirgleydar.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class CommandGM {

	
	public static void execute(CommandSender sender, String[] args) throws Exception{
	  if(sender instanceof Player){
		  Player p = (Player)sender;
		  
		  switch(args.length){
		  case 0:
			  if(p.hasPermission("gm.self")){
				  if(p.getGameMode().equals(GameMode.CREATIVE)){
					  p.setGameMode(GameMode.SURVIVAL);
					  Main.sendMessage("gm", "Your gamemode is now %survival&", p);
				  }else{
					  p.setGameMode(GameMode.CREATIVE);
					  Main.sendMessage("gm", "Your gamemode is now %creative&", p);
				  }
			  }
			  break;
			  
		  case 1:
			  if(p.hasPermission("gm.self")){
				  switch(args[0]){
				  
				  case "0":
				  case "survival":
				  case "s":
					  p.setGameMode(GameMode.SURVIVAL);
					  Main.sendMessage("gm", "Your gamemode is now %survival&", p);
					  break;
					  
				  case "1":
				  case "creative":
				  case "c":
					  p.setGameMode(GameMode.CREATIVE);
					  Main.sendMessage("gm", "Your gamemode is now %creative&", p);
					  break;
				 
				  case "2":
				  case "adventure":
				  case "a":
					  p.setGameMode(GameMode.ADVENTURE);
					  Main.sendMessage("gm", "Your gamemode is now %adventure&", p);
					  break;
					  
					  default:
						  if(p.hasPermission("gm.others")){
							  if(Bukkit.getPlayer(args[0]).isOnline()){
								  if(Bukkit.getPlayer(args[0]).getGameMode().equals(GameMode.CREATIVE)){
									  Bukkit.getPlayer(args[0]).setGameMode(GameMode.SURVIVAL);
									  Main.sendMessage("gm", "You changed%" +args[0]+ "&'s gamemode to %survival&", p);
									  Main.sendMessage("gm", "Your gamemode was changed to %survival&", Bukkit.getPlayer(args[0]));
								  }else{
									  Bukkit.getPlayer(args[0]).setGameMode(GameMode.CREATIVE);
									  Main.sendMessage("gm", "You changed%" +args[0]+ "&'s gamemode to %creative&", p);
									  Main.sendMessage("gm", "Your gamemode was changed to %creative&", Bukkit.getPlayer(args[0]));
								  }
							  }else{
								  throw new InvalidDataException("gm", args[0], "gamemode or player");
							  }
						  }
				  }
			  }
			  
			  break;
			  
		  case 2:
			  break;
			  
		  default:
				 
		  }
		  
		}
	  }
	
}


