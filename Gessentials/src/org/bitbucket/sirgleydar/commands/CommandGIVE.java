package org.bitbucket.sirgleydar.commands;

import org.bitbucket.sirgleydar.exceptions.ArgumentException;
import org.bitbucket.sirgleydar.exceptions.InvalidDataException;
import org.bitbucket.sirgleydar.exceptions.NoPermissionException;
import org.bitbucket.sirgleydar.exceptions.PlayerNotFoundException;
import org.bitbucket.sirgleydar.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class CommandGIVE {

	public static void execute(CommandSender sender, String[] args) throws Exception{
		if(sender instanceof Player){
			Player p = (Player)sender;
			
			switch(args.length){
			case 1:
				String mat = args[0].toUpperCase();
				
				if(p.hasPermission("give.self") && p.hasPermission("give.s." + mat)){
					try{
						p.getInventory().addItem(new ItemStack(Material.valueOf(mat)));
						Main.sendMessage("give", "you gave yourself %64 " + mat + "&", p);
					}catch(Exception e){
						throw new InvalidDataException("give", mat, "material");
					}
				}else{
					throw new NoPermissionException();
				}
				
				break;
				
			case 2:
				mat = args[0].toUpperCase();
				int count; 
				
				try{
					count = Integer.parseInt(args[1]);
				}catch(NumberFormatException e){
					throw new InvalidDataException("give", args[1], "number");
				}
					
				if(p.hasPermission("give.self") && p.hasPermission("give.i." + mat)){
					
					try{
						p.getInventory().addItem(new ItemStack(Material.valueOf(mat), count));
						Main.sendMessage("give", "you gave yourself %" + args[1] + " "+ mat + "&", p);
					}catch(Exception e){
						throw new InvalidDataException("give", mat, "material");
					}
				}else{
					throw new NoPermissionException();
				}
				break;
				
			case 3:
				Player pp = Bukkit.getPlayer(args[0]);
				mat = args[1].toUpperCase();
					try{
						count = Integer.getInteger(args[2]);
					}catch(NumberFormatException e){
						throw new InvalidDataException("give", args[2], "number");
					}
				
				if(!pp.isOnline()){
					throw new PlayerNotFoundException("give", pp.getDisplayName());
				}
				
				if(p.hasPermission("give.others") && p.hasPermission("give.o." + mat)){
					try{
						pp.getInventory().addItem(new ItemStack(Material.valueOf(mat), count));
						Main.sendMessage("give", "you were given  %" + args[1] + " "+ mat + "& by " + pp.getDisplayName(), pp);
						Main.sendMessage("give", "you gave " + pp.getDisplayName() +" %" + args[1] + " "+ mat + "&", p);
					}catch(Exception e){
						throw new InvalidDataException("give", mat, "material");
					}
					
				}else{
					throw new NoPermissionException();
				}
				break;
				
			default:
				throw new ArgumentException("give");
				
			}
		}else{
			Player pp = Bukkit.getPlayer(args[0]);
			String mat = args[1].toUpperCase();
			int count; 
				try{
					count = Integer.getInteger(args[2]);
				}catch(NumberFormatException e){
					throw new InvalidDataException("give", args[2], "number");
				}
			
			if(!pp.isOnline()){
				throw new PlayerNotFoundException("give", pp.getDisplayName());
			}
			
			try{
				pp.getInventory().addItem(new ItemStack(Material.valueOf(mat), count));
				Main.sendMessage("give", "you were given  %" + args[1] + " "+ mat + "& by " + pp.getDisplayName(), pp);
				Main.log.info("");
			}catch(Exception e){
				throw new InvalidDataException("give", mat, "material");
			}
			
		}
	
	}
}


