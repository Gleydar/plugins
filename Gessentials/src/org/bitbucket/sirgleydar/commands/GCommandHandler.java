package org.bitbucket.sirgleydar.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class GCommandHandler {

	public GCommandHandler(){

	}
	
	public static boolean handleCommand(CommandSender sender, Command cmd, String[] args) throws Exception{
	    
		try{
			switch(cmd.getLabel().toLowerCase()){
				case "tp":
				case "teleport": CommandTP.execute(sender, args);
					break;
				case "ban": CommandBAN.execute(sender, args);
					break;
				case "unban":
				case "pardon": CommandUNBAN.execute(sender, args);
					break;
				case "give": CommandGIVE.execute(sender, args);
					break;
				case "gm":
				case "gamemode": CommandGM.execute(sender, args);
					break;
			}
		}catch(Exception e){
			sender.sendMessage(e.getMessage());
		}
		
		return true;
	}
	
}


