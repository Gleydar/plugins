package org.bitbucket.sirgleydar.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * 
 * 
 * @author Gleydar
 * 
 */

public class CommandTP {

	public static void execute(CommandSender sender, String[] args){
		if(sender instanceof Player){
			Player p =  (Player)sender;
			
			if(args.length == 1){
				
				if(p.hasPermission("teleport.self")){
					if(Bukkit.getPlayer(args[0]) != null){
						Player pl = Bukkit.getPlayer(args[0]);
						
						p.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.BLUE +"Du wirst teleportiert.");
						p.teleport(pl.getLocation());
						
					}else{
						p.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.GOLD +"Der gew�nschte Spieler konnte nicht gefunden werden.");
					}
							
				}else{
					p.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.RED +"Nein. Ich m�chte das nicht.");
				}
				
			}else if(args.length == 2){
				if(p.hasPermission("teleport.other")){
					
					if(Bukkit.getPlayer(args[0]) != null && Bukkit.getPlayer(args[1]) != null){
						Player pl = Bukkit.getPlayer(args[0]);
						Player pp = Bukkit.getPlayer(args[1]);
						
						pp.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.BLUE +"Du wirst teleportiert.");
						pl.teleport(pl.getLocation());
						
					}else{
						p.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.GOLD +"Der gew�nschte Spieler konnte nicht gefunden werden.");
					}
					
				}else{
					p.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.RED +"Nein. Ich m�chte das nicht.");
				}
			}else{
				p.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.RED +"Unzul�ssige Argumente");
			}
		}else{
			if(args.length == 2){
				
				if(Bukkit.getPlayer(args[0]) != null && Bukkit.getPlayer(args[1]) != null){
					Player pl = Bukkit.getPlayer(args[0]);
					Player pp = Bukkit.getPlayer(args[1]);
					
					pp.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.BLUE +"Du wirst teleportiert.");
					pl.teleport(pl.getLocation());
					
				}else{
					sender.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.GOLD +"Der gew�nschte Spieler konnte nicht gefunden werden.");
				}
				
			}else{
				sender.sendMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE +"Teleport" + ChatColor.DARK_BLUE+"] " + ChatColor.RED +"Unzul�ssige Argumente");
			}
		}
		
	}
	
}


